import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';
import { VerDesarrolladorComponent } from './componentes/ver-desarrollador/ver-desarrollador.component';
import { MainService } from './servicios/main.service';
import { MensajesApp } from './Util/mensajes-app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private api: MainService,
    private _snackBar: MatSnackBar
  ) {
    this.autenticar();
  }

  goPage = (pagina: string) => { this.router.navigateByUrl('/' + pagina); };
  verDesarrollador = () => { this.dialog.open(VerDesarrolladorComponent) };

  autenticar = () => {
    this.api.autenticar().subscribe(
      data => {
        console.log(data, 'data')
        localStorage.setItem('tkSession', (data as any).token);
        this.router.navigate(['../libros']);
      },
      error => {
        console.log(error, 'error/onLogin')
        this._snackBar.open(MensajesApp.ERROR_LOGIN, MensajesApp.TITULO_CONTINUAR, {
          duration: 5 * 1000,
        });
      }
    );
  };
}
