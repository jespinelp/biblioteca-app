export interface IAutor {
    Id: number;
    Nombre: string;
    Apellidos: string;
    Fecha_nacimiento: Date;
}

export class Autor implements IAutor{
    Id: number = 0;
    Nombre: string = '';
    Apellidos: string = '';
    Fecha_nacimiento: Date = new Date(1900,0,1);
}