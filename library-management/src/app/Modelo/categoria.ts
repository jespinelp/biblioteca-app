export interface ICategoria {
    Id: number;
    Nombre: string;
    Descripcion: string;
}

export class Categoria {
    Id: number;
    Nombre: string;
    Descripcion: string;
}