export interface IParametroGuardarLibro {
    Id: number;
    Nombre: string;
    ISBN: string;
    IdAutor: number;
    IdCategoria: number;
}

export class ParametroGuardarLibro implements IParametroGuardarLibro {
    Id: number = 0;
    Nombre: string = '';
    ISBN: string = '';
    IdAutor: number = 0;
    IdCategoria: number = 0;
}