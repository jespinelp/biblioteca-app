import { Categoria } from './categoria';
import { Autor } from './autor';

export interface ILibro {
    Id: number;
    Nombre: string;
    ISBN: string;
    Autor: Autor;
    Categoria: Categoria;
}

export class Libro implements ILibro {
    Id: number = 0;
    Nombre: string = '';
    ISBN: string = '';
    Autor: Autor = null;
    Categoria: Categoria = null;
       
    constructor() { }
}