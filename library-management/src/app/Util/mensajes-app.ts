export class MensajesApp{
    
    public static readonly TITULO_CONTINUAR: string = 'Continuar';
    public static readonly TITULO_MSGS_ERROR: string = 'LMS [Error]';
    public static readonly TITULO_MSGS_INFO: string = 'LMS [Información]';
    public static readonly MSG_MSGS_INFO: string = 'Datos actualizados satisfactoriamente';
    
    //Login
    public static readonly ERROR_LOGIN: string = 'Error al autenticar la app.';

}