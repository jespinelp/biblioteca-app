export class Conexion{
    // public static readonly UrlApi : string = 'http://localhost:55834/api/';
    public static readonly UrlApi : string = 'https://library-servicios.azurewebsites.net/api/';
    //Autenticar App
    public static readonly autenticador: string = 'main/autenticar_app';
    //Categorias
    public static readonly ConsultarCategorias: string = 'Categoria/';
    public static readonly GuardarCategoria: string = 'Categoria/guardar_categoria';
    public static readonly EliminarCategoria: string = 'Categoria/eliminar_categoria';
     //Autores
     public static readonly ConsultarAutores: string = 'Autor/';
     public static readonly GuardarAutor: string = 'Autor/guardar_autor';
     public static readonly EliminarAutor: string = 'Autor/eliminar_autor';
      //Categorias
    public static readonly ConsultarLibros: string = 'Libro/obtener_libros';
    public static readonly GuardarLibro: string = 'Libro/guardar_libro';
    public static readonly EliminarLibro: string = 'Libro/eliminar_libro';
}