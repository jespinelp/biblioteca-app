import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Conexion } from '../Util/conexion';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(private http: HttpClient) {
    console.log('**BlueSoft_Services_Works**');
  }

  autenticar = () => { return this.http.post(Conexion.UrlApi.concat(Conexion.autenticador),'', httpOptions) };

}