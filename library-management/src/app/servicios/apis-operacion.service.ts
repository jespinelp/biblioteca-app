import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Conexion } from '../Util/conexion';
import { Categoria } from '../Modelo/categoria';
import { Autor } from '../Modelo/autor';
import { Libro } from '../Modelo/libro';
import { ParametroGuardarLibro } from '../Modelo/parametro-guardar-libro';

@Injectable({
  providedIn: 'root'
})
export class ApisOperacionService {

  constructor(private http: HttpClient) {
    console.log('**ApisOperacionService_Works**');
  }

  callGet(urlSvc): Observable<any> {


    let v_headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    v_headers = v_headers.set('Authorization', 'Bearer ' + localStorage.getItem('tkSession'));

    const httpOptions = {
      headers: v_headers
    }
    return this.http.get(Conexion.UrlApi.concat(urlSvc), httpOptions)
  }

  callPost(urlSvc, params): Observable<any> {

    console.log(params,'params/post')

    let v_headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    v_headers = v_headers.set('Authorization', 'Bearer ' + localStorage.getItem('tkSession'));

    const httpOptions = {
      headers: v_headers
    }
    return this.http.post(Conexion.UrlApi.concat(urlSvc), params, httpOptions)
  }

  callDelete(urlSvc): Observable<any> {


    let v_headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    v_headers = v_headers.set('Authorization', 'Bearer ' + localStorage.getItem('tkSession'));

    const httpOptions = {
      headers: v_headers
    }
    return this.http.delete(Conexion.UrlApi.concat(urlSvc), httpOptions)
  }


  //Categorías
  consultarCategorias = (id: number = 0) => { return this.callGet(Conexion.ConsultarCategorias + id.toString())};
  guardarCategoria = (pCategoria: Categoria) => { return this.callPost(Conexion.GuardarCategoria, pCategoria)};
  eliminarCategoria = (id: number) => { return this.callDelete(Conexion.EliminarCategoria.concat('?id='+id.toString()))};
  
  //Autores
  consultarAutores = (id: number = 0) => { return this.callGet(Conexion.ConsultarAutores + id.toString())};
  guardarAutor = (pAutor: Autor) => { return this.callPost(Conexion.GuardarAutor, pAutor)};
  eliminarAutor = (id: number) => { return this.callDelete(Conexion.EliminarAutor.concat('?id='+id.toString()))};

  //Libros
  consultarLibros = (id: number = 0) => { return this.callGet(Conexion.ConsultarLibros)};
  guardarLibro = (pLibro: ParametroGuardarLibro) => { return this.callPost(Conexion.GuardarLibro, pLibro)};
  eliminarLibro = (id: number) => { return this.callDelete(Conexion.EliminarLibro.concat('?id='+id.toString()))};

}
