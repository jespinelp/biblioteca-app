import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 

import { NgSelectModule } from '@ng-select/ng-select';


import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonToggleModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDividerModule,
  MatGridListModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatIconModule,
  MatCardModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatSelectModule,
  MatRadioModule,
  MatTableModule,
  MatDialogModule, DateAdapter, MAT_DATE_LOCALE
} from "@angular/material";
import { FlexLayoutModule } from "@angular/flex-layout";


import { MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MatMomentDateModule } from '@angular/material-moment-adapter';


// Componentes
import { AutoresComponent } from './componentes/autores/autores.component';
import { AutorGetComponent } from './componentes/autores/autor-get/autor-get.component';
import { CategoriasComponent } from './componentes/categorias/categorias.component';
import { CategoriaGetComponent } from './componentes/categorias/categoria-get/categoria-get.component';
import { LibrosComponent } from './componentes/libros/libros.component';
import { LibroGetComponent } from './componentes/libros/libro-get/libro-get.component';
import { VerDesarrolladorComponent } from './componentes/ver-desarrollador/ver-desarrollador.component';
//Servicios
import { MainService } from './servicios/main.service';
import { ApisOperacionService } from './servicios/apis-operacion.service';
import { ConfirmarMsgComponent } from './componentes/confirmar-msg/confirmar-msg.component';


const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};


@NgModule({
  declarations: [
    AppComponent,
    AutoresComponent,
    AutorGetComponent,
    CategoriasComponent,
    CategoriaGetComponent,
    LibrosComponent,
    LibroGetComponent,
    VerDesarrolladorComponent,
    ConfirmarMsgComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule, FormsModule, MatButtonModule, MatMenuModule, MatDialogModule, MatSnackBarModule,
    MatFormFieldModule, MatInputModule, MatCardModule, MatTableModule, MatPaginatorModule, MatSortModule,
    MatSidenavModule, MatIconModule, MatToolbarModule, MatListModule, FlexLayoutModule, MatIconModule,
    MatAutocompleteModule, MatSelectModule,
    MatDatepickerModule,       
    MatNativeDateModule,       
    MatMomentDateModule, 
    NgSelectModule
  ],
  providers: [ {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
  {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
  MainService, ApisOperacionService],
  bootstrap: [AppComponent],
  entryComponents: [
    VerDesarrolladorComponent, AutorGetComponent, CategoriaGetComponent, LibroGetComponent, ConfirmarMsgComponent
  ],
  
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
