import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibrosComponent } from './componentes/libros/libros.component';
import { AutoresComponent } from './componentes/autores/autores.component';
import { CategoriasComponent } from './componentes/categorias/categorias.component';


const routes: Routes = [
  {path:'libros', component: LibrosComponent},
  {path:'autores', component: AutoresComponent},
  {path:'categorias', component: CategoriasComponent},
  // {
  //     path: '',
  //     redirectTo: '/libros',
  //     pathMatch: 'full'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
