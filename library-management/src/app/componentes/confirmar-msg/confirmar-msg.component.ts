import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-confirmar-msg',
  templateUrl: './confirmar-msg.component.html',
  styleUrls: ['./confirmar-msg.component.scss']
})
export class ConfirmarMsgComponent implements OnInit {

  titulo: string = '';
  mensaje: string = '';
  valor: boolean = true;
  constructor(
    public dialogRef: MatDialogRef<ConfirmarMsgComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.titulo = data.titulo;
    this.mensaje = data.mensaje;
  }
  ngOnInit() {
  }

}
