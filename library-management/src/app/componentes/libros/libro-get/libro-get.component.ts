import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { ApisOperacionService } from 'src/app/servicios/apis-operacion.service';
import { MensajesApp } from 'src/app/Util/mensajes-app';
import { ILibro, Libro } from 'src/app/Modelo/libro';
import { IAutor } from 'src/app/Modelo/autor';
import { ICategoria } from 'src/app/Modelo/categoria';
import { NgSelectConfig } from '@ng-select/ng-select';
import { ParametroGuardarLibro } from 'src/app/Modelo/parametro-guardar-libro';


export interface User {
  name: string;
}

@Component({
  selector: 'app-libro-get',
  templateUrl: './libro-get.component.html',
  styleUrls: ['./libro-get.component.scss']
})
export class LibroGetComponent implements OnInit {

  obj: any = {};

  lista_autores: any[] = [];
  lista_categorias: ICategoria[] = [];

  autorSelected: any;
  categoriaSelected: number;

  guardando: boolean = false;



  constructor(
    public dialogRef: MatDialogRef<LibroGetComponent>,
    private apis: ApisOperacionService,
    private _snackBar: MatSnackBar,
    private config: NgSelectConfig,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    console.log(data, 'data/libro-get')
    this.obj = data.libro;
    this.lista_autores = data.lista_autores.map((i) => { i.fullName = i.nombre + ' ' + i.apellidos; return i; });;
    this.lista_categorias = data.lista_categorias;
    this.config.notFoundText = 'No hay autores registrados';
    this.config.appendTo = 'body';
  }


  ngOnInit() {
    if(this.obj.id > 0){
      this.lista_autores.forEach(element => {
        if (element.id == this.obj.autor.id) {
          this.autorSelected = element;
        }
      });
      this.categoriaSelected = this.obj.categoria.id;
    }
    
  }


  save() {
    console.log(this.obj, 'save')
    this.guardando = true;

    const parametros: ParametroGuardarLibro = {
      Id: this.obj.id,
      Nombre: this.obj.nombre,
      ISBN: this.obj.isbn,
      IdAutor: this.autorSelected.id,
      IdCategoria: this.categoriaSelected
    }

    this.apis.guardarLibro(parametros)
      .subscribe((data: any) => {
        this.guardando = false;
        console.log(data);

        if (data.error == 'OK') {
          this.dialogRef.close({ recargar: true });
        } else {
          this._snackBar.open(data.mensaje, MensajesApp.TITULO_CONTINUAR, {
            duration: 8000,
          });
        }


      }),
      (error) => {
        this.guardando = false;

        console.log(error, 'erorrrrorroro');
        this._snackBar.open(error, MensajesApp.TITULO_CONTINUAR, {
          duration: 5 * 1000,
        });
      };
  }

  esLibroInvalido(): boolean {
    return (this.autorSelected == null || this.obj.nombre == '' || (!(this.categoriaSelected > 0)))
  }


}
