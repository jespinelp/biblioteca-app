import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ILibro, Libro } from 'src/app/Modelo/libro';
import { ApisOperacionService } from 'src/app/servicios/apis-operacion.service';
import { MensajesApp } from 'src/app/Util/mensajes-app';
import { MatSnackBar, MatDialogConfig, MatDialog } from '@angular/material';
import { LibroGetComponent } from './libro-get/libro-get.component';
import { ConfirmarMsgComponent } from '../confirmar-msg/confirmar-msg.component';
import { forkJoin } from 'rxjs';
import { ICategoria } from 'src/app/Modelo/categoria';
import { IAutor } from 'src/app/Modelo/autor';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.scss']
})
export class LibrosComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'autor', 'categoria', 'isbn', 'opciones'];
  dataSource: MatTableDataSource<ILibro> = new MatTableDataSource<ILibro>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  guardando: boolean = false;
  data_categorias: ICategoria[] = [];
  data_autores: IAutor[] = [];

  constructor(
    private apis: ApisOperacionService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog) {
    this.consultarListas();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  obtenerLibros = () => {
    this.guardando = true;
    this.apis.consultarLibros().subscribe(
      data => {
        this.guardando = false;
        console.log(data, 'data');
        let libros: ILibro[] = data as Libro[];
        this.dataSource = new MatTableDataSource(libros);

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this._snackBar.open(MensajesApp.MSG_MSGS_INFO, MensajesApp.TITULO_CONTINUAR, {
          duration: 5 * 1000, horizontalPosition: "end", verticalPosition: "top"
        });
      },
      error => {
        this.guardando = false;
        console.log(error, 'error/onLogin')
        this._snackBar.open(MensajesApp.ERROR_LOGIN, MensajesApp.TITULO_CONTINUAR, {
          duration: 5 * 1000,
        });
      }
    );
  };


  consultarListas() {
    this.guardando = true;
    forkJoin([
      this.apis.consultarAutores(),
      this.apis.consultarCategorias(),
    ]).subscribe(
      ([data_autores, data_categorias]) => {

        this.obtenerLibros();

        this.data_autores = data_autores as IAutor[];
        this.data_categorias = data_categorias as ICategoria[];
      },
      error => {
        console.log(error, 'error');
        this._snackBar.open(error, MensajesApp.TITULO_CONTINUAR, {
          duration: 5 * 1000,
        });
      }
    );
  }

  getLibro(obj: ILibro = new Libro()) {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '605px';
    dialogConfig.disableClose = true;
    dialogConfig.data = { libro: obj, lista_autores: this.data_autores, lista_categorias: this.data_categorias }


    this.dialog.open(LibroGetComponent, dialogConfig).afterClosed()
      .subscribe(response => {
        console.log(response);
        if (response) {
          console.log(response);
          if (response.recargar) {
            this.obtenerLibros();
          }
        } else {
          console.log('nada');
        }
      });
  }


  eliminar(obj) {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.disableClose = true;
    dialogConfig.data = {
      titulo: 'Eliminar Libro',
      mensaje: 'Confirma eliminar el libro seleccionado [' + obj.id + '][' + obj.nombre + ']?'
    }


    this.dialog.open(ConfirmarMsgComponent, dialogConfig).afterClosed()
      .subscribe(response => {
        console.log(response);
        if (response) {

          this.apis.eliminarLibro(obj.id).subscribe(
            data => {
              console.log(data);

              if (data.error == 'OK') {
                this._snackBar.open(data.mensaje, MensajesApp.TITULO_CONTINUAR, {
                  duration: 8000,
                });
                this.obtenerLibros();
              } else {
                this._snackBar.open(data.mensaje, MensajesApp.TITULO_CONTINUAR, {
                  duration: 8000,
                });
              }


            }),
            (error) => {
              this.guardando = false;

              console.log(error, 'error');
              this._snackBar.open(error, MensajesApp.TITULO_CONTINUAR, {
                duration: 5 * 1000,
              });
            };
        }
      });
  }

}
