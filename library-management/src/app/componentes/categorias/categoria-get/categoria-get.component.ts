import { Component, OnInit, Inject } from '@angular/core';
import { ICategoria, Categoria } from 'src/app/Modelo/categoria';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { ApisOperacionService } from 'src/app/servicios/apis-operacion.service';
import { MensajesApp } from 'src/app/Util/mensajes-app';

@Component({
  selector: 'app-categoria-get',
  templateUrl: './categoria-get.component.html',
  styleUrls: ['./categoria-get.component.scss']
})
export class CategoriaGetComponent implements OnInit {

  obj: any = {};
  guardando: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<CategoriaGetComponent>,
    private apis: ApisOperacionService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.obj = data;
  }


  save() {
    this.guardando = true;
    this.apis.guardarCategoria(this.obj)
      .subscribe((data: any) => {
        this.guardando = false;
        console.log(data);

        if (data.error == 'OK') {
          this.dialogRef.close({ recargar: true });
        } else {
          this._snackBar.open(data.mensaje, MensajesApp.TITULO_CONTINUAR, {
            duration: 8000,
          });
        }


      }),
      (error) => {
        this.guardando = false;

        console.log(error, 'erorrrrorroro');
        this._snackBar.open(error, MensajesApp.TITULO_CONTINUAR, {
          duration: 5 * 1000,
        });
      };
  }


  ngOnInit() {
  }

  
}
