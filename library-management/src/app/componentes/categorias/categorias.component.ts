import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar, MatDialog, MatDialogConfig } from '@angular/material';
import { ICategoria, Categoria } from 'src/app/Modelo/categoria';
import { ApisOperacionService } from 'src/app/servicios/apis-operacion.service';
import { MensajesApp } from 'src/app/Util/mensajes-app';
import { CategoriaGetComponent } from './categoria-get/categoria-get.component';
import { ConfirmarMsgComponent } from '../confirmar-msg/confirmar-msg.component';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasComponent implements OnInit {
  displayedColumns: string[] = ['nombre', 'descripcion', 'opciones'];
  dataSource: MatTableDataSource<ICategoria> = new MatTableDataSource<ICategoria>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  guardando: boolean = false;

  constructor(
    private apis: ApisOperacionService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog, ) {
    this.obtenerCategorias();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  obtenerCategorias = () => {
    this.guardando= true;
    this.apis.consultarCategorias().subscribe(
      data => {
        this.guardando= false;
        console.log(data, 'data');
        let categorias: ICategoria[] = data as Categoria[];
        this.dataSource = new MatTableDataSource(categorias);

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this._snackBar.open(MensajesApp.MSG_MSGS_INFO, MensajesApp.TITULO_CONTINUAR, {
          duration: 5 * 1000, horizontalPosition: "end", verticalPosition: "top"
        });
      },
      error => {
        this.guardando= false;

        console.log(error, 'error/onLogin')
        this._snackBar.open(error, MensajesApp.TITULO_CONTINUAR, {
          duration: 5 * 1000,
        });
      }
    );
  };


  getCategoria(obj: ICategoria = new Categoria()) {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '605px';
    dialogConfig.disableClose = true;
    dialogConfig.data = obj


    this.dialog.open(CategoriaGetComponent, dialogConfig).afterClosed()
      .subscribe(response => {
        console.log(response);
        if (response) {
          console.log(response);
          if (response.recargar) {
            this.obtenerCategorias();
          }
        } else {
          console.log('nada');
        }
      });
  }


  eliminar(obj) {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.disableClose = true;
    dialogConfig.data = {
      titulo: 'Eliminar Categoría',
      mensaje: 'Confirma eliminar la categoría seleccionada [' + obj.id + '][' + obj.nombre + ']?'
    }


    this.dialog.open(ConfirmarMsgComponent, dialogConfig).afterClosed()
      .subscribe(response => {
        console.log(response);
        if (response) {

          this.apis.eliminarCategoria(obj.id).subscribe(
            data => {
              console.log(data);

              if (data.error == 'OK') {
                this._snackBar.open(data.mensaje, MensajesApp.TITULO_CONTINUAR, {
                  duration: 8000,
                });
                this.obtenerCategorias();
              } else {
                this._snackBar.open(data.mensaje, MensajesApp.TITULO_CONTINUAR, {
                  duration: 8000,
                });
              }


            }),
            (error) => {
              this.guardando = false;

              console.log(error, 'error');
              this._snackBar.open(error, MensajesApp.TITULO_CONTINUAR, {
                duration: 5 * 1000,
              });
            };
        }
      });
    }

}
