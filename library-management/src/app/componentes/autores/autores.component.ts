import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { IAutor, Autor } from 'src/app/Modelo/autor';
import { ApisOperacionService } from 'src/app/servicios/apis-operacion.service';
import { MensajesApp } from 'src/app/Util/mensajes-app';
import { MatSnackBar, MatDialogConfig, MatDialog } from '@angular/material';
import { AutorGetComponent } from './autor-get/autor-get.component';
import { ConfirmarMsgComponent } from '../confirmar-msg/confirmar-msg.component';


@Component({
  selector: 'app-autores',
  templateUrl: './autores.component.html',
  styleUrls: ['./autores.component.scss']
})
export class AutoresComponent implements OnInit {
  displayedColumns: string[] = ['nombre', 'apellidos', 'fecha_nacimiento', 'opciones'];
  dataSource: MatTableDataSource<IAutor> = new MatTableDataSource<IAutor>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  guardando: boolean = false;

  constructor(
    private apis: ApisOperacionService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog, ) {
    this.obtenerAutores();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  obtenerAutores = () => {

    this.apis.consultarAutores().subscribe(
      data => {
        console.log(data, 'data');
        let libros: IAutor[] = data as Autor[];
        this.dataSource = new MatTableDataSource(libros);

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this._snackBar.open(MensajesApp.MSG_MSGS_INFO, MensajesApp.TITULO_CONTINUAR, {
          duration: 5 * 1000, horizontalPosition: "end", verticalPosition: "top"
        });
      },
      error => {
        console.log(error, 'error/onLogin')
        this._snackBar.open(error, MensajesApp.TITULO_CONTINUAR, {
          duration: 5 * 1000,
        });
      }
    );
  };


  getAutor(obj: IAutor = new Autor()) {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '605px';
    dialogConfig.disableClose = true;
    dialogConfig.data = obj


    this.dialog.open(AutorGetComponent, dialogConfig).afterClosed()
      .subscribe(response => {
        console.log(response);
        if (response) {
          console.log(response);
          if (response.recargar) {
            this.obtenerAutores();
          }
        } else {
          console.log('nada');
        }
      });
  }


  eliminar(obj) {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.disableClose = true;
    dialogConfig.data = {
      titulo: 'Eliminar Autor',
      mensaje: 'Confirma eliminar el autor seleccionado [' + obj.id + '][' + obj.nombre + ' ' + obj.apellidos + ']?'
    }


    this.dialog.open(ConfirmarMsgComponent, dialogConfig).afterClosed()
      .subscribe(response => {
        console.log(response);
        if (response) {

          this.apis.eliminarAutor(obj.id).subscribe(
            data => {
              console.log(data);

              if (data.error == 'OK') {
                this._snackBar.open(data.mensaje, MensajesApp.TITULO_CONTINUAR, {
                  duration: 8000,
                });
                this.obtenerAutores();
              } else {
                this._snackBar.open(data.mensaje, MensajesApp.TITULO_CONTINUAR, {
                  duration: 8000,
                });
              }


            }),
            (error) => {
              this.guardando = false;

              console.log(error, 'erorrrrorroro');
              this._snackBar.open(error, MensajesApp.TITULO_CONTINUAR, {
                duration: 5 * 1000,
              });
            };
        }
      });
    }

}
