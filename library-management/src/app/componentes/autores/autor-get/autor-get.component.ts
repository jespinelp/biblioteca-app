import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Autor, IAutor } from 'src/app/Modelo/autor';
import { ApisOperacionService } from 'src/app/servicios/apis-operacion.service';
import { MensajesApp } from 'src/app/Util/mensajes-app';

@Component({
  selector: 'app-autor-get',
  templateUrl: './autor-get.component.html',
  styleUrls: ['./autor-get.component.scss']
})
export class AutorGetComponent implements OnInit {

  obj: any = {};
  guardando: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<AutorGetComponent>,
    private apis: ApisOperacionService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) data
  ) { 
    this.obj = data;
  }

  
  save() {
    this.guardando = true;
    this.apis.guardarAutor(this.obj)
    .subscribe((data: any) => {
      this.guardando = false;
      console.log(data);

      if(data.error == 'OK'){
        this.dialogRef.close({recargar:true});
      }else{
        this._snackBar.open(data.mensaje, MensajesApp.TITULO_CONTINUAR, {
          duration: 8000,
        });
      }

      
    }),
    (error) => {
      this.guardando = false;

      console.log(error,'erorrrrorroro');
      this._snackBar.open(error, MensajesApp.TITULO_CONTINUAR, {
        duration: 5 * 1000,
      });
    };
  }


  ngOnInit() {
  }

}
