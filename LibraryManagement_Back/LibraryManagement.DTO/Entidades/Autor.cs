﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryManagement.DTO.Entidades
{
    public class Autor
    {
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Nombre { get; set; }
        [Required]
        [Column(TypeName = "varchar(200)")]
        public string Apellidos { get; set; }
        public DateTime Fecha_nacimiento { get; set; }
    }
}
