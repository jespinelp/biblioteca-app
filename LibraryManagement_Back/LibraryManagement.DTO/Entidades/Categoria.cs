﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryManagement.DTO.Entidades
{
    public class Categoria
    {
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "varchar(300)")]
        public string Nombre { get; set; }

        [Column(TypeName = "varchar(2500)")]
        public string Descripcion { get; set; }
    }
}
