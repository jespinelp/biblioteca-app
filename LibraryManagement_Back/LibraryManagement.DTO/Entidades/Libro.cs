﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryManagement.DTO.Entidades
{
    public class Libro
    {

        public int Id { get; set; }

        [Required]
        [Column(TypeName = "varchar(500)")]
        public string Nombre { get; set; }

        [Required]
        public Autor Autor { get; set; }

        [Required]
        public Categoria Categoria { get; set; }

        [Column(TypeName = "varchar(13)")]
        public string ISBN { get; set; }

    }
}
