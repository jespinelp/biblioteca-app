﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryManagement.DTO.Modelo
{
    public class Resultado
    {
        public Resultado() { }
        public Resultado(int id, string error, string mensaje)
        {
            Id = id;
            Error = error;
            Mensaje = mensaje;
        }

        public int Id { get; set; }
        public string Error { get; set; }
        public string Mensaje { get; set; }
    }
}
