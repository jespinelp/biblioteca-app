﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryManagement.DTO.Modelo
{
    public class ParametroGuardarLibro
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string ISBN { get; set; }
        public int IdAutor { get; set; }
        public int IdCategoria { get; set; }
    }
}
