﻿using LibraryManagement.DAL.Utilidades;
using LibraryManagement.DTO.Entidades;
using LibraryManagement.DTO.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryManagement.DAL.Repositorio
{
    public class LibroRepositorio
    {
        public List<Libro> ObtenerLibros(int id, string nombre, int id_categoria, int id_autor)
        {
            using (var ctx = new BlueSoft_DbContext())
            {
                return (from lib in ctx.Libros.Where(lib => (lib.Id == id || id == 0)
                                                && (lib.Nombre.Contains(nombre) || nombre == null || nombre == "")
                                                && (lib.Autor.Id == id_autor || id_autor == 0)
                                                && (lib.Categoria.Id == id_categoria || id_categoria == 0)
                                        )
                        join aut in ctx.Autores on lib.Autor.Id equals aut.Id
                        join cate in ctx.Categorias on lib.Categoria.Id equals cate.Id
                        select new Libro()
                        {
                            Id = lib.Id,
                            Nombre = lib.Nombre,
                            ISBN = lib.ISBN,
                            Autor = aut,
                            Categoria = cate
                        }).ToList();
            }
        }

        public Resultado GuardarLibro(ParametroGuardarLibro libro)
        {
            var oResultado = new Resultado();
            int id_registro = libro.Id;
            try
            {
                using (var ctx = new BlueSoft_DbContext())
                {
                    if (libro.Id > 0)
                    {
                        var libro_db = ctx.Libros.Where(cat => cat.Id == libro.Id).ToList().FirstOrDefault();
                        libro_db.Nombre = libro.Nombre;
                        libro_db.ISBN = libro.ISBN;
                        libro_db.Autor = ctx.Autores.Where(aut => aut.Id == libro.IdAutor).ToList().FirstOrDefault();
                        libro_db.Categoria = ctx.Categorias.Where(cat => cat.Id == libro.IdCategoria).ToList().FirstOrDefault();
                    }
                    else
                    {
                        var libro_db = new Libro();
                        libro_db.Nombre = libro.Nombre;
                        libro_db.ISBN = libro.ISBN;
                        libro_db.Autor = ctx.Autores.Where(aut => aut.Id == libro.IdAutor).ToList().FirstOrDefault();
                        libro_db.Categoria = ctx.Categorias.Where(cat => cat.Id == libro.IdCategoria).ToList().FirstOrDefault();
                        ctx.Add(libro_db);
                        id_registro = libro_db.Id;
                    }
                    ctx.SaveChanges();
                    oResultado = new Resultado(oResultado.Id = id_registro, "OK", "Libro guardado satisfactoriamente [" + libro.Nombre + "]");
                }
            }
            catch (Exception e)
            {
                oResultado = new Resultado(oResultado.Id = -1, "ERROR", "Error al guardar el libro. Msg: " + e.Message);
            }
            return oResultado;
        }


        public Resultado EliminarLibro(int id_libro)
        {
            var oResultado = new Resultado();
            try
            {
                using (var ctx = new BlueSoft_DbContext())
                {
                    ctx.Libros.Remove(new Libro() { Id = id_libro });
                    ctx.SaveChanges();
                    oResultado = new Resultado(oResultado.Id = id_libro, "OK", "Libro eliminado satisfactoriamente.");
                }
            }
            catch (Exception e)
            {
                oResultado = new Resultado(oResultado.Id = -1, "ERROR", "Error al eliminar el Libro. Msg: " + e.Message);
            }
            return oResultado;
        }
    }
}
