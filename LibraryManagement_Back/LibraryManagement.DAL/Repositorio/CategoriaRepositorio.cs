﻿using LibraryManagement.DAL.Utilidades;
using LibraryManagement.DTO.Entidades;
using LibraryManagement.DTO.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryManagement.DAL.Repositorio
{
    public class CategoriaRepositorio
    {
        public List<Categoria> ObtenerCategorias(int id)
        {
            using (var ctx = new BlueSoft_DbContext())
            {
                return ctx.Categorias.Where(cat => cat.Id == id || id == 0).ToList();
            }
        }

        public Resultado GuardarCategoria(Categoria categoria)
        {
            var oResultado = new Resultado();
            try
            {
                using (var ctx = new BlueSoft_DbContext())
                {
                    if (categoria.Id > 0)
                    {
                        var categoria_db = ctx.Categorias.Where(cat => cat.Id == categoria.Id).ToList().FirstOrDefault();
                        categoria_db.Nombre = categoria.Nombre;
                        categoria_db.Descripcion = categoria.Descripcion;
                    }
                    else
                    {
                        ctx.Add(categoria);
                    }
                    ctx.SaveChanges();
                    oResultado = new Resultado(oResultado.Id = categoria.Id, "OK", "Categoría guardado satisfactoriamente [" + categoria.Nombre + "]");
                }
            }
            catch (Exception e)
            {
                oResultado = new Resultado(oResultado.Id = -1, "ERROR", "Error al guardar la categoría. Msg: " + e.Message);
            }
            return oResultado;
        }
        
        public Resultado EliminarCategoria(int id_categoria)
        {
            var oResultado = new Resultado();
            try
            {
                using (var ctx = new BlueSoft_DbContext())
                {
                    ctx.Categorias.Remove(new Categoria() { Id = id_categoria });
                    ctx.SaveChanges();
                    oResultado = new Resultado(oResultado.Id = id_categoria, "OK", "Categoría eliminada satisfactoriamente.");
                }
            }
            catch (Exception e)
            {
                oResultado = new Resultado(oResultado.Id = -1, "ERROR", "Error al eliminar la categoría. Msg: " + e.Message);
            }
            return oResultado;
        }

    }
}
