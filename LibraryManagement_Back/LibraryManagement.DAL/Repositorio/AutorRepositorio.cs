﻿using LibraryManagement.DAL.Utilidades;
using LibraryManagement.DTO.Entidades;
using LibraryManagement.DTO.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryManagement.DAL.Repositorio
{
    public class AutorRepositorio
    {
        public List<Autor> ObtenerAutores(int id)
        {
            using (var ctx = new BlueSoft_DbContext())
            {
                return ctx.Autores.Where(aut => aut.Id == id || id == 0).ToList();
            }
        }

        public Resultado GuardarAutor(Autor autor)
        {
            var oResultado = new Resultado();
            try
            {
                using (var ctx = new BlueSoft_DbContext())
                {
                    if (autor.Id > 0)
                    {
                        var autor_db = ctx.Autores.Where(cat => cat.Id == autor.Id).ToList().FirstOrDefault();
                        autor_db.Nombre = autor.Nombre;
                        autor_db.Apellidos = autor.Apellidos;
                        autor_db.Fecha_nacimiento = autor.Fecha_nacimiento;
                    }
                    else
                    {
                        ctx.Add(autor);
                    }
                    ctx.SaveChanges();
                    oResultado = new Resultado(oResultado.Id = autor.Id, "OK", "Autor guardado satisfactoriamente [" + autor.Nombre + "]");
                }
            }
            catch (Exception e)
            {
                oResultado = new Resultado(oResultado.Id = -1, "ERROR", "Error al guardar el autor. Msg: " + e.Message);
            }
            return oResultado;
        }

        public Resultado EliminarAutor(int id_autor)
        {
            var oResultado = new Resultado();
            try
            {
                using (var ctx = new BlueSoft_DbContext())
                {
                    ctx.Autores.Remove(new Autor() { Id = id_autor });
                    ctx.SaveChanges();
                    oResultado = new Resultado(oResultado.Id = id_autor, "OK", "Autor eliminado satisfactoriamente.");
                }
            }
            catch (Exception e)
            {
                oResultado = new Resultado(oResultado.Id = -1, "ERROR", "Error al eliminar el autor. Msg: " + e.Message);
            }
            return oResultado;
        }



    }
}

