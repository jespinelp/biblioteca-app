﻿namespace LibraryManagement.DAL.Utilidades
{
    public static class Utilidades
    {
        public static string ConexionString { get; set; }

        public static void Configuracion(string conexion_string) => ConexionString = conexion_string;
    }
}
