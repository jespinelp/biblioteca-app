﻿using LibraryManagement.DTO.Entidades;
using Microsoft.EntityFrameworkCore;

namespace LibraryManagement.DAL.Utilidades
{
    public class BlueSoft_DbContext : DbContext
    {
        /// <summary>
        /// Configuración de nuestro DbContext
        /// </summary>
        /// <param name="optionsBuilder"></param>
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseSqlServer("Data Source=186.31.83.47\\SQLEXPRESSMT;Initial Catalog=LibManApp; user=j2o; password=123456");
        //Comentar esta línea y descomentar la anterior para ejecutar la migración 
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseSqlServer(Utilidades.ConexionString);

        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Autor> Autores { get; set; }
        public DbSet<Libro> Libros { get; set; }
    }
}
