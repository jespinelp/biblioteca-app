﻿using log4net;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Api.Utilidades
{
    public class UtilidadesApi
    {
        public IConfiguration Configuration { get; set; }

        public UtilidadesApi()
        {
            var builder = new ConfigurationBuilder()
               .AddJsonFile("appSettings.json");
            Configuration = builder.Build();
        }

        public string GetConfiguracion(string atributo) => Configuration[atributo];

        public string GetStringConnection(string tagNameDb = "BS_LibMan_Db") => GetConfiguracion("ConnectionStrings:" + tagNameDb);

        public void BlueSoft_Log(string msg)
        {
            LogManager.GetLogger(typeof(Program)).Info(msg);
        }
    }
}
