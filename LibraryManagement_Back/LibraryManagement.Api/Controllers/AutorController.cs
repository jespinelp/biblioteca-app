﻿using LibraryManagement.Api.Utilidades;
using LibraryManagement.BLL;
using LibraryManagement.DTO.Entidades;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Api.Controllers
{
    [EnableCors("BlueSoft_Cors")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AutorController : ControllerBase
    {
        [HttpGet("{id?}")]
        public ActionResult Get(int id = 0)
        {
            new UtilidadesApi().BlueSoft_Log("Call api [ObtenerAutores] Param id = " + id.ToString());
            return Ok(new AutorNegocio().ObtenerAutores(id));
        }

        [HttpPost]
        [Route("guardar_autor")]
        public ActionResult GuardarAutor([FromBody] Autor autor)
        {
            new UtilidadesApi().BlueSoft_Log("Call api [GuardarAutor] Param Autor: "
                                                                                + "Id" + autor.Id.ToString()
                                                                                + ";Nombre" + autor.Nombre
                                                                                + ";Apellidos" + autor.Apellidos
                                                                                + ";Fecha_Nacimiento" + autor.Fecha_nacimiento.ToString()
                                            );
            return Ok(new AutorNegocio().GuardarAutor(autor));
        }

        [HttpDelete]
        [Route("eliminar_autor")]
        public ActionResult EliminarAutor(int id)
        {
            new UtilidadesApi().BlueSoft_Log("Call api [EliminarAutor] Autor Id: " + id.ToString());
            return Ok(new AutorNegocio().EliminarAutor(id));
        }
    }
}
