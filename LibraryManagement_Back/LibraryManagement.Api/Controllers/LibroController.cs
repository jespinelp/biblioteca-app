﻿using LibraryManagement.Api.Utilidades;
using LibraryManagement.BLL;
using LibraryManagement.DTO.Modelo;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Api.Controllers
{
    [EnableCors("BlueSoft_Cors")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class LibroController : ControllerBase
    {
        [HttpGet]
        [Route("validar_token")]
        public ActionResult ValidadorToken()
        {
            return Ok(true);
        }

        [HttpGet]
        [Route("obtener_libros")]
        public ActionResult Cargar(int id, string nombre, int id_categoria, int id_autor)
        {
            new UtilidadesApi().BlueSoft_Log("Call api [ObtenerLibros] Param id = " + id.ToString());
            return Ok(new LibroNegocio().ObtenerLibros(id, nombre, id_categoria, id_autor));
        }

        [HttpPost]
        [Route("guardar_libro")]
        public ActionResult GuardarLibro([FromBody] ParametroGuardarLibro pLibroData)
        {
            new UtilidadesApi().BlueSoft_Log("Call api [GuardarLibro] Param libro: "
                                                                                + "Id" + pLibroData.Id.ToString()
                                                                                + ";Nombre" + pLibroData.Nombre
                                                                                + ";ISBN" + pLibroData.ISBN
                                            );
            return Ok(new LibroNegocio().GuardarLibro(pLibroData));
        }

        [HttpDelete]
        [Route("eliminar_libro")]
        public ActionResult EliminarLibro(int id)
        {
            new UtilidadesApi().BlueSoft_Log("Call api [EliminarLibro] Libro Id: " + id.ToString());
            return Ok(new LibroNegocio().EliminarLibro(id));
        }
    }
}
