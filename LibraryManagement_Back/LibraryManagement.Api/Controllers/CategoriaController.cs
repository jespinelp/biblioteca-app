﻿using LibraryManagement.Api.Utilidades;
using LibraryManagement.BLL;
using LibraryManagement.DTO.Entidades;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace LibraryManagement.Api.Controllers
{
    [EnableCors("BlueSoft_Cors")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CategoriaController : ControllerBase
    {
        [HttpGet("{id?}")]
        public ActionResult Get(int id = 0)
        {
            new UtilidadesApi().BlueSoft_Log("Call api [ObtenerCategorias] Param id = " + id.ToString());
            return Ok(new CategoriaNegocio().ObtenerCategorias(id));
        }

        [HttpPost]
        [Route("guardar_categoria")]
        public ActionResult GuardarCategoria([FromBody] Categoria categoria)
        {
            new UtilidadesApi().BlueSoft_Log("Call api [GuardarCategoria] Param categoria: "
                                                                                + "Id" + categoria.Id.ToString()
                                                                                + ";Nombre" + categoria.Nombre
                                                                                + ";Descripcion" + categoria.Descripcion
                                            );
            return Ok(new CategoriaNegocio().GuardarCategoria(categoria));
        }

        [HttpDelete]
        [Route("eliminar_categoria")]
        public ActionResult EliminarCategoria(int id)
        {
            new UtilidadesApi().BlueSoft_Log("Call api [EliminarCategoria] Categoría Id: " + id.ToString());
            return Ok(new CategoriaNegocio().EliminarCategoria(id));
        }
    }
}
