﻿using LibraryManagement.Api.Utilidades;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Api.Controllers
{
    [EnableCors("BlueSoft_Cors")]
    [Route("api/[controller]")]
    public class MainController : ControllerBase
    {

        private readonly IConfiguration _configuracion;
        public MainController(
            IConfiguration configuracion
            )
        {
            this._configuracion = configuracion;
        }

        [HttpGet("{par?}")]
        public ActionResult<IEnumerable<string>> Get(string par = "")
        {
            return new string[]{ "Autor: Jeffrey Alonso Espinel Pérez",
                                 "Version: 1.0.0",
                                 "DB Connection:"+( par =="db92" ? (new UtilidadesApi()).GetStringConnection():"*****")
                               };
        }

        private IActionResult GenerarToken()
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, "app"),
                new Claim("usuario", "app"),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuracion["LlaveSecreta"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddHours(8);
            JwtSecurityToken token = new JwtSecurityToken(
                                issuer: "bluesoft.com", audience: "bluesoft.com",
                                claims: claims, expires: expiration, signingCredentials: creds
                            );

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = expiration
            });
        }

        [HttpPost]
        [Route("autenticar_app")]
        public IActionResult AutenticarApp()
        {
            try
            {
                new UtilidadesApi().BlueSoft_Log("Autenticar Aplicación ");
                return GenerarToken();
            }
            catch (Exception e)
            {
                return BadRequest("Error: " + e.Message);
            }
        }
    }
}
