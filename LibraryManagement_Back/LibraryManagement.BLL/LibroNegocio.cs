﻿using LibraryManagement.DAL.Repositorio;
using LibraryManagement.DTO.Entidades;
using LibraryManagement.DTO.Modelo;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryManagement.BLL
{
    public class LibroNegocio
    {
        public List<Libro> ObtenerLibros(int id, string nombre, int id_categoria, int id_autor)
                                            => new LibroRepositorio().ObtenerLibros(id, nombre, id_categoria, id_autor);

        public Resultado GuardarLibro(ParametroGuardarLibro libro) => new LibroRepositorio().GuardarLibro(libro);

        public Resultado EliminarLibro(int id) => new LibroRepositorio().EliminarLibro(id);
    }
}
