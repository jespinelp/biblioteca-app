﻿using LibraryManagement.DAL.Repositorio;
using LibraryManagement.DTO.Entidades;
using LibraryManagement.DTO.Modelo;
using System.Collections.Generic;

namespace LibraryManagement.BLL
{
    public class CategoriaNegocio
    {
        public List<Categoria> ObtenerCategorias(int id) => new CategoriaRepositorio().ObtenerCategorias(id);

        public Resultado GuardarCategoria(Categoria categoria) => new CategoriaRepositorio().GuardarCategoria(categoria);

        public Resultado EliminarCategoria(int id) => new CategoriaRepositorio().EliminarCategoria(id);
    }
}
