﻿using LibraryManagement.DAL.Repositorio;
using LibraryManagement.DTO.Entidades;
using LibraryManagement.DTO.Modelo;
using System.Collections.Generic;

namespace LibraryManagement.BLL
{
    public class AutorNegocio
    {
        public List<Autor> ObtenerAutores(int id) => new AutorRepositorio().ObtenerAutores(id);

        public Resultado GuardarAutor(Autor categoria) => new AutorRepositorio().GuardarAutor(categoria);

        public Resultado EliminarAutor(int id) => new AutorRepositorio().EliminarAutor(id);
    }
}
